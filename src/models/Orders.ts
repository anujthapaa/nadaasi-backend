import * as mongoose from "mongoose";

export interface Order extends Document {
  cartItems: object;
  address: object;
  message: string;
  total: number
}

const OrderSchema = new mongoose.Schema({
  cartItems: {
    type: Object
  },
  address: {
    type: Object,
  },
  message: {
    type: String,
  },
  total: {
    type: Number,
  }
});

export default mongoose.model("Order", OrderSchema);
