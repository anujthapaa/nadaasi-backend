import * as mongoose from "mongoose";

export interface Iproduct extends Document {
  Product_id: number;
  name: string;
  price: number;
  DressType: string;
  availableSizes: Array<string>;
}

const ProductSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  detail: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  DressType: {
    type: String,
    required: true
  },
  availableSizes: [{
    type: String,
    required: true
  }]
});

export default mongoose.model("Product", ProductSchema);
