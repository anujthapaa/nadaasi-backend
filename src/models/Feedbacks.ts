import * as mongoose from "mongoose";

export interface Feedback extends Document {
  name: string;
  subject: string;
  email: string;
  message: string;
}

const FeedbackSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  subject: {
    type: String,
    required: true
  },
  email: {
    type: String,
    min: [4, "Too short, min is 4 characters"],
    max: [32, "Too long, max is 32 characters"],
    unique: true,
    lowercase: true,
    required: "Email is required",
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
  },
  message: {
    type: String,
    required: true
  }
});

export default mongoose.model("Feedback", FeedbackSchema);
