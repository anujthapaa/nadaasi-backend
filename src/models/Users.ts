const mongoose = require("mongoose");
const bcrypt = require('bcrypt');

export interface Iuser extends Document {
  id: number;
  username: string;
  password: string;
  role: string;
}

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    min: [4, "Too short, min is 4 characters"],
    max: [32, "Too long, max is 32 characters"]
  },
  email: {
    type: String,
    min: [4, "Too short, min is 4 characters"],
    max: [32, "Too long, max is 32 characters"],
    unique: true,
    lowercase: true,
    required: "Email is required",
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
  },
  password: {
    type: String,
    required: true,
    min: [4, "Too short, min is 4 characters"],
    max: [32, "Too long, max is 32 characters"],
  },
  role: {
    type: String,
    required: true
  }
});

userSchema.pre('save', function(next) {
  const user = this;

  if(user.isModified('password')) {
    bcrypt.genSalt(10, function(err, salt) {
      if(err) return next(err);

      bcrypt.hash(user.password, salt, function(err, hash) {
        if(err) return next(err);
        user.password = hash;
        next();
      })
    })

  } else {
    next()
  }
});

userSchema.methods.comparePassword = function(candidatePassword, checkpassword) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch){
    if(err) return checkpassword(err);
    checkpassword(null, isMatch);
  })
}

export default mongoose.model("Iuser", userSchema);
