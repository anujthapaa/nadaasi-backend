//This is the user route for authentication, login and logout

import * as express from "express";
import IUser from "../../models/Users";

const router: any = express.Router();

//@Request Type = Post
//@desc Route to Post data
//Private Route
router.post("/", (req: express.Request, res: express.Response) => {
  let user = new IUser(req.body);
  
  user.save(function (err) {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  })
});

//@Request Type = Post
//@desc Route to get one user data
//Private Route
router.post("/login", (req: express.Request, res: express.Response) => {
  
  IUser.findOne({'email': req.body.email}, (err, user) => {
    
    if(!user) res.json({message: 'Login failed, user not found'})
    
    user.comparePassword(req.body.password, (err, isMatch) => {
      if(err) throw err;
      if(!isMatch) return res.status(400).json({
        message: 'Wrong Password'
      });
      res.status(200).json(user)
    })
  })
});

//@Request Type = GET
//@desc Route to fetch users data
// Public route
router.get("/", (req: express.Request, res: express.Response) => {
  let users = IUser.find((err: any, users: any) => {
    if (err) {
      res.send(err)
    } else {
      res.send(users)      
    }
  })
});

module.exports = router;    