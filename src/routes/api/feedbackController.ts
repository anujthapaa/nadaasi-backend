//This is the feedback route for user feedbacks posting and viewing

import * as express from "express";
import Feedback from "../../models/Feedbacks";

const router: any = express.Router();

//@Request Type = Post
//@desc Route to Post data
//Private Route
// router.post("/", (req: express.Request, res: express.Response) => {
//   let feedback = new Feedback(req.body);
//   console.log(feedback);
  
//   feedback.save(function (err) {
//     if (err) return res.json({ success: false, error: err });
//     return res.json({ success: true });
//   })
// });

//@Request Type = GET
//@desc Route to fetch feedback data
// Public route
router.get("/", (req: express.Request, res: express.Response) => {
  let feedbacks = Feedback.find((err: any, feedbacks: any) => {
    if (err) {
      res.send(err)
    } else {
      res.send(feedbacks)
    }
  })
});

module.exports = router;    