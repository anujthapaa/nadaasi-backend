//This is the orders route for orders posting and viewing

import * as express from "express";
import Order from "../../models/Orders";

const router: any = express.Router();

// @Request Type = Post
// @desc Route to Post data
// Private Route
router.post("/", (req: express.Request, res: express.Response) => {
  let order = new Order(req.body);
  
  order.save(function (err) {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  })
});

//@Request Type = GET
//@desc Route to fetch feedback data
// Public route
router.get("/", (req: express.Request, res: express.Response) => {
  let orders = Order.find((err: any, orders: any) => {
    if (err) {
      res.send(err)
    } else {
      res.send(orders)
    }
  })
});

module.exports = router;    