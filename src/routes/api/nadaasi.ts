import * as express from "express";
import Product from "../../models/Products";
import Order from "../../models/Orders";
import IUser from "../../models/Users";

const router: any = express.Router();

//@Request Type = GET
//@desc Route to fecth data
// Public route
router.get("/", (req: express.Request, res: express.Response) => {
  let products = Product.find((err: any, products: any) => {
    if (err) {
      res.send(err)
    } else {
      res.send(products)
    }
  })
});

//@Request Type = GET
//id  item id(number)
//@desc Route to fecth data with id
// Public route
router.get("/:id", (req: express.Request, res: express.Response) => {
  let product = Product.findById(req.params.id, (err: any, product: any) => {
    if (err) {
      res.send(err)
    } else {
      res.send(product);
    }
  })
});

//@Request Type = Put
//id  item id(number)
//@desc Route to Edit data
// Public route
router.put("/:id", (req: express.Request, res: express.Response) => {
        Product.findByIdAndUpdate(req.params.id, req.body, (err: any, product: any) => {
          if (err) {
            res.send(err)
          } else {
            console.log(req.params.id + req.body.name + "successfully updated");
            res.json({ success: true });
          }
        })
});

//@Request Type = Post
//@desc Route to Post data
//Private Route
router.post("/", (req: express.Request, res: express.Response) => {
  // IUser.findById(req.params.id, (err: any, currentUser: any) => {
  //   if (err) {
  //     res.send(err);
  //   } else {
  //     if (currentUser.role == "admin") {

  let product = new Product(req.body);

  product.save(function (err) {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  })
});

//@Request Type = Post
//@desc Route to Post Order
//Private Route
router.post("/orders", (req: express.Request, res: express.Response) => {
  let order = new Order(req.body);
  console.log(order);


  // order.save(function (err) {
  //   if (err) return res.json({ success: false, error: err });
  //   return res.json({ success: true });
  // })
});

//@Request Type = Delete
//@desc Route to Post data
//Private Route
router.delete("/:id", (req: express.Request, res: express.Response) => {
  // IUser.findById(req.params.id, (err: any, currentUser: any) => {
  //   if (err) {
  //     res.send(err);
  //   } else {
  //     if (currentUser.role == "admin") {

  Product.deleteOne({ "name": req.params.id }, (err: any) => {
    if (err) {
      res.send(err)
    } else {
      res.send("Successfully deleted the product: " + req.params.id)
    }
  });
  //     } else {
  //       res.send("Sorry, only admins are allowed to add products");
  //     }
  //   }
  // });

});

module.exports = router;
