import * as express from "express";
import connect from "../config/db";

const PORT = process.env.PORT || 8000;
const app: express.Application = express();
const path = require('path');
const cors = require('cors');

connect();

app.use(cors());
app.use(express.json());
// we are specifying the build directory as another public directory
app.use(express.static(path.join(__dirname, 'public/react/demo-react')));

// var bodyParser = require("body-parser");
// app.use(bodyParser.urlencoded({ extended: true }));


app.get("/", (req: express.Request, res: express.Response) => {
    res.send(`Backend is running on ${process.env.PORT}`)
    //sending the index file inside the build directory as response
    //res.sendFile(path.join(__dirname, 'public/react/demo-react/build'));
});

app.use("/nadassi", require("./routes/api/nadaasi"));
app.use("/user", require("./routes/api/userController"));
app.use("/feedback", require("./routes/api/feedbackController"));
app.use("/order", require("./routes/api/orderController"));

app.listen(PORT, () => console.log(`Running in ${PORT}`));

