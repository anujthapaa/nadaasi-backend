import * as mongoose from "mongoose";
require('dotenv').config()

// import config from "config";
// const db: any = config.get("mongoURI");
const mongoURI: any =
  `mongodb+srv://${process.env.MONGOURI_USER_NAME}:${process.env.MONGOURI_PASSWORD}@cluster0-ljbgz.gcp.mongodb.net/test?retryWrites=true&w=majority`;

export default () => {
  const connect = () => {
    mongoose
      .connect(mongoURI, { useNewUrlParser: true })
      .then(() => {        
        return console.log(`Successfully connected to database`);
      })
      .catch(error => {
        console.log("Error connecting to database: ", error);
        return process.exit(1);
      });
  };
  connect();

  mongoose.connection.on("disconnected", connect);
};
